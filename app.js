var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var config = require('./config.json');
var app = express();

app.use(bodyParser.json());
//  Se agrega para caracteres especiales
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/gruposComida', require('./api/gruposComida'));
app.use('/platillo', require('./api/platillos'));

app.listen(config.puertoLocal, function(){
  console.log('Servidor corriendo en el puerto ' + config.puertoLocal);
})

module.exports = app;
