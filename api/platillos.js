var express = require('express');
var request = require('request');
var moment = require('moment');
var router = express.Router();
//  Archivo de configuración
var config = require('../config');

/* Menu y orden para mesa */
router.get('/', function(req, res, next) {
  console.log('Solicitó platillos');
  var token = req.query.token;
  var idgrupo = req.query.idgrupo;
  var urlToken = 'http://' + config.servidorAutenticacion.direccionIP + ':' + config.servidorAutenticacion.puerto + config.servidorAutenticacion.listaEndpoints.end_verifica + '?token=' + token;
  console.log('Platillos: ' + urlToken);

  if(idgrupo !== undefined)
  {
    console.log(urlToken);
    request({url:urlToken, json:true}, function(errorVer, respVer, bodyVer){
      if (errorVer)
      {
        console.log("Error en la verificación del token");
        res.statusCode(404).send({detail: 'Error, server not found'});
      }
      else if (respVer.statusCode==200)
      {
        var urlL = 'http://' + config.servidorWS.direccionIP + ':' + config.servidorWS.puerto + config.servidorWS.listaEndpoints.categorias + '?idgrupo=' + idgrupo;
        console.log(urlL);
        request({url:urlL, json:true}, function(errorCat, respCat, bodyCat){
          if (errorCat)
          {
            console.log("Error en la obtención de las categorías del menu");
            res.status(404).send({detail: 'Error, server not found'});
          }
          else
          {
            console.log('bodyCat');
            console.log(bodyCat);
            if(bodyCat.length > 0)
            {
              categorias(bodyCat, function(platillos){
                console.log('--- Categorias ---');
                console.log(platillos);
                getPlatillos(1, function(respuesta){
                  res.status(200).send(platillos);
                }); // getPlatillos
              }); // categorias
            }
            else
            {
              var menu = {
                platillos :0
              };
              res.status(respVer.statusCode).send(menu);
            }
          }
        }); // request categorias
      }
      else
      {
        res.status(respVer.statusCode).send({detail : 'Token invalido'});
      }
    }); // request para verificar token
  }
  else
  {
    res.status(400).send({detail : 'Falta seleccionar el tipo de tiempo'});
  }
}); // get

/* Obtiene las categorías correspondientes y las retorna en un arreglo */
var categorias = function(categ, callback){
  var platillos = [];
  var longitud =categ.length;
  console.log('Tamaño categoria: ' + categ.length);
  categ.forEach(function(item, index){
    getPlatillos(item.idcategoria, function(plaResp){
      if(plaResp.status !== 200)
      {
        console.log(plaResp.descripcion);
      }
      else
      {
        var jPlatillos = {
          idcategoria : item.idcategoria,
          name : item.name,
          platillos : plaResp.platillos
        };
        platillos.push(jPlatillos);
        if (index == longitud-1)
          callback(platillos);
      }
    });//platillos
  });//forEach
};

// Obtiene los platillos correspondientes en base a la categoría que se selecciona
var getPlatillos = function(categoria, callback){
  var urlL = 'http://' + config.servidorWS.direccionIP + ':' + config.servidorWS.puerto + config.servidorWS.listaEndpoints.platillos + '?idcategoria=' + categoria;
  console.log('getPlatillos ' + urlL);
  // console.log(urlL);
  request({url:urlL, json:true}, function(error, resp, body){
    var jResp = {
      status : 200,
      platillos : [],
      descripcion : ''
    };
    if (error)
    {
      console.log("Error en la obtención de los platillos del menu");
      // retornamos json con error y array vacio
      jResp.status = 404;
      jResp.descripcion = 'Error, server no found';
      callback(jResp);
    }
    else
    {
      var tam = resp.body.length;
      console.log('Platillos .---------------------- ' + tam);
      // console.log(resp.body);
      var arrayPlatillo = [];
      console.log('Obtuvimos ' + tam)
      if(tam > 0)
      {
        // recorrer arreglo y quitarle idplatillo, idcategoria
        resp.body.forEach(function(i, index)
        {
          // console.log('----');
          var platillo =  {
            idplatillo : i.idplatillo,
            nombre : i.name,
            precio : i.precio,
            imagen : i.imagen,
            descripcion : i.descripcion,
            tiempo : i.tiempoPreparacion
          };

          arrayPlatillo.push(platillo);
          if(index == tam -1)
          {
            console.log('No: ' + index);
            var jTotal = {
              status : 200,
              platillos : arrayPlatillo,
              descripcion : ''
            };
            callback(jTotal);
          }
        }); //forEach
      }
      else
      {
        jResp.platillo = [];
        callback(jResp);
      }
    }
  }); //request
};


module.exports = router;
