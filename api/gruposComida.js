var express = require('express');
var request = require('request');
var moment = require('moment');
var router = express.Router();
//  Archivo de configuración
var config = require('../config');

/* Menu y orden para mesa */
router.get('/', function(req, res, next) {
  var token = req.query.token;
  console.log('Ya llegó la petición de tiempo');
  request({url:'http://' + config.servidorAutenticacion.direccionIP + ':' + config.servidorAutenticacion.puerto + config.servidorAutenticacion.listaEndpoints.end_verifica + '?token=' + token, json : true}, function(errorVer, respVer, bodyVer){
    if (errorVer)
    {
      console.log("Error en la verificación del token");
      res.status(404).send({detail: 'Error, authenticacion server not found'});
    }
    else if (respVer.statusCode==200)
    {
      console.log('Respondió 200: ' + JSON.stringify(bodyVer));
      var sucursal = bodyVer.sucursal;
      if(sucursal !== undefined)
      {
        request({url:'http://' + config.servidorWS.direccionIP + ':' + config.servidorWS.puerto + config.servidorWS.listaEndpoints.grupo + '?sucursal=' + sucursal, json:true}, function(errorMenu, respMenu, bodyMenu){
          if (errorMenu)
          {
            console.log("Error en la obtención de las opciones del menú");
            res.status(404).send({detail: 'Error, groups server not found'});
          }
          else
          {
            if(bodyMenu.length == 0)
            {
              console.log('No encontramos opciones de tiempo en esa sucursal... informar a usuario');
              res.status(404).send({ detail : 'Esta sucursal no tiene tiempos de comida asignados.' });
            }
            else
            {
              opcMenuSuc(bodyMenu, function(opcMenu){
                console.log(opcMenu);
                res.status(200).send(opcMenu);
              }); //opcMenuSuc
            }
          }
        }); // request de sucursal
      }
      else
      {
        console.log('No tenemos definida la sucursal');
          res.status(respVer.statusCode).send(req.body);
      }
    }
    else
    {
      console.log(respVer.statusCode);
      res.status(respVer.statusCode).send(req.body);
    }
  }); // Request verificacion
}); //get

//Funcion que retorna el objeto con las opciones del menu acorde al horario solicitado.
//Parámetro de entrada: bodyMenu
//Parámetro de salida: opcMenu

var opcMenuSuc = function (bodyMenu, callback){

  var opcMenu = [];
  var fechaHoraIni;
  var fechaHoraFin;
  var fechaAct = moment().format('YYYY-MM-DD');
  var fechaHoraAct = moment().format('YYYY-MM-DD HH:mm:ss');

  var longitud = bodyMenu.length;

  bodyMenu.forEach(function(item, index){
    fechaHoraIni = fechaAct + " " + item.horaIni;
    fechaHoraFin = fechaAct + " " + item.horaFin;

    console.log("Inicio: " + fechaHoraIni + ", Fin: " + fechaHoraFin + ", Actual: " + fechaHoraAct );

    //Se valida que la fecha de solicitud de menu este dentro de la hora inicial y final de alguna de las opciones del menu.
    if (Date.parse(fechaHoraAct) > Date.parse(fechaHoraIni)) {
      if (Date.parse(fechaHoraAct) < Date.parse(fechaHoraFin)) {
        var jopcMenu = {
          idgrupo : item.idgrupo,
          name : item.name,
          imagen : item.imagen
        };
        opcMenu.push(jopcMenu);
      }
    }
    if (index == longitud-1){
      callback(opcMenu);
    }
  }); //forEach
};


module.exports = router;
